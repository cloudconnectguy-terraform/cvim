# Enter the region, connection ID and VLAN ID from the AWS portal for the respective DX connection

variable "region" {
}

variable "dx1a-connection_id" {
}

variable "dx1a-vlan_id" {
}

variable "dx1b-connection_id" {
}

variable "dx1b-vlan_id" {
}

variable "dx2a-connection_id" {
}

variable "dx2a-vlan_id" {
}

variable "dx2b-connection_id" {
}

variable "dx2b-vlan_id" {
}

variable "bgp-auth-key" {
}
