# Terraform Script to build AWS infrastructure needed for CVIM Upgrades
# Run the following commands before terraform apply
# export AWS_ACCESS_KEY_ID="anaccesskey"
# export AWS_SECRET_ACCESS_KEY="asecretkey"
# export AWS_DEFAULT_REGION="aregion"
# Note: if you add a space before the export command it will not be saved to history file on disk

# Build VPC-A Infrastructure

provider "aws" {
  region = var.region
}

resource "aws_vpc" "vpc-a" {
  cidr_block            = "10.100.0.0/24"
  instance_tenancy      = "default"
  enable_dns_hostnames  = true

  tags = {
    Name = "VPC-A"
  }
}

resource "aws_subnet" "subnet-a" {
  vpc_id     = aws_vpc.vpc-a.id
  cidr_block = "10.100.0.0/27"

  tags = {
    Name = "Subnet-A"
  }
}

resource "aws_route_table" "route-a" {
  vpc_id = aws_vpc.vpc-a.id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-a.id
  }

  tags = {
    Name = "Route-A"
  }
}

resource "aws_route_table_association" "route-assoc-a" {
  subnet_id      = aws_subnet.subnet-a.id
  route_table_id = aws_route_table.route-a.id
}

resource "aws_internet_gateway" "igw-a" {
  vpc_id = aws_vpc.vpc-a.id
  tags = {
    Name = "IGW-A"
  }
}

resource "aws_vpn_gateway" "vgw-a" {
  vpc_id            = aws_vpc.vpc-a.id
  amazon_side_asn   = "65100"

  tags = {
    Name = "VGW-A"
  }
}

resource "aws_vpn_gateway_route_propagation" "route-prop-a" {
  vpn_gateway_id = aws_vpn_gateway.vgw-a.id
  route_table_id = aws_route_table.route-a.id
}

resource "aws_security_group" "sg-a" {
  name        = "Security Group-A"
  description = "Allow inbound traffic for VPC-A"
  vpc_id      = aws_vpc.vpc-a.id

  ingress {
    description = "HTTP from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "RDP from VPC"
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Any from 10/8"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow SG-A"
  }
}

# Data Source queries latest Windows instance and passes to next block

data "aws_ami" "prtg_image" {
  owners = ["self"]
  most_recent = true
  filter {
    name = "name"
    values = ["windows_server_prtg"]
  }
}

resource "aws_instance" "instance-a" {
    ami                      = data.aws_ami.prtg_image.id
    instance_type            = "t2.micro"
    subnet_id                = aws_subnet.subnet-a.id
    key_name                 = "cvim_upgrade_key"
    private_ip               = "10.100.0.15"
    vpc_security_group_ids   = [aws_security_group.sg-a.id]

tags = {
    Name = "Instance-1A"
  }
}

resource "aws_eip" "eip-a" {
  instance = aws_instance.instance-a.id
  vpc      = true
}

# Build VPC-B Infrastructure

resource "aws_vpc" "vpc-b" {
  cidr_block            = "10.200.0.0/24"
  instance_tenancy      = "default"
  enable_dns_hostnames  = true

  tags = {
    Name = "VPC-B"
  }
}

resource "aws_subnet" "subnet-b" {
  vpc_id     = aws_vpc.vpc-b.id
  cidr_block = "10.200.0.0/27"

  tags = {
    Name = "Subnet-B"
  }
}

resource "aws_route_table" "route-b" {
  vpc_id = aws_vpc.vpc-b.id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-b.id
  }

  tags = {
    Name = "Route-B"
  }
}

resource "aws_route_table_association" "route-assoc-b" {
  subnet_id      = aws_subnet.subnet-b.id
  route_table_id = aws_route_table.route-b.id
}

resource "aws_internet_gateway" "igw-b" {
  vpc_id = aws_vpc.vpc-b.id
  tags = {
    Name = "IGW-B"
  }
}

resource "aws_vpn_gateway" "vgw-b" {
  vpc_id            = aws_vpc.vpc-b.id
  amazon_side_asn   = "65200"

  tags = {
    Name = "VGW-B"
  }
}

resource "aws_vpn_gateway_route_propagation" "route-prop-b" {
  vpn_gateway_id = aws_vpn_gateway.vgw-b.id
  route_table_id = aws_route_table.route-b.id
}

resource "aws_security_group" "sg-b" {
  name        = "Security Group-B"
  description = "Allow inbound traffic for VPC-B"
  vpc_id      = aws_vpc.vpc-b.id

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Any from 10/8"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow SG-B"
  }
}

# Data Source queries latest Amazon Linux instance and passes to next block

data "aws_ssm_parameter" "latest-instance-b" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"

 }

resource "aws_instance" "instance-b" {
    ami                      = data.aws_ssm_parameter.latest-instance-b.value
    instance_type            = "t2.micro"
    subnet_id                = aws_subnet.subnet-b.id
    key_name                 = "cvim_upgrade_key"
    private_ip               = "10.200.0.15"
    vpc_security_group_ids   = [aws_security_group.sg-b.id]

tags = {
    Name = "Instance-2B"
  }
}

resource "aws_eip" "eip-b" {
  instance = aws_instance.instance-b.id
  vpc      = true
}

resource "aws_dx_private_virtual_interface" "vif-1a" {
  
  connection_id = var.dx1a-connection_id
  name               = "VIF-1A"
  vlan               = var.dx1a-vlan_id
  address_family     = "ipv4"
  bgp_asn            = 65000
  vpn_gateway_id     = aws_vpn_gateway.vgw-a.id
  bgp_auth_key       = var.bgp-auth-key
  customer_address   = "10.0.0.1/30"
  amazon_address     = "10.0.0.2/30"
}

resource "aws_dx_private_virtual_interface" "vif-1b" {
  
  connection_id      = var.dx1b-connection_id
  name               = "VIF-1B"
  vlan               = var.dx1b-vlan_id
  address_family     = "ipv4"
  bgp_asn            = 65000
  vpn_gateway_id     = aws_vpn_gateway.vgw-a.id
  bgp_auth_key       = var.bgp-auth-key
  customer_address   = "10.0.0.5/30"
  amazon_address     = "10.0.0.6/30"
}

resource "aws_dx_private_virtual_interface" "vif-2a" {
  
  connection_id      = var.dx2a-connection_id
  name               = "VIF-2A"
  vlan               = var.dx2a-vlan_id
  address_family     = "ipv4"
  bgp_asn            = 65000
  vpn_gateway_id     = aws_vpn_gateway.vgw-b.id
  bgp_auth_key       = var.bgp-auth-key
  customer_address   = "10.0.0.9/30"
  amazon_address     = "10.0.0.10/30"
}

resource "aws_dx_private_virtual_interface" "vif-2b" {
  
  connection_id      = var.dx2b-connection_id
  name               = "VIF-2B"
  vlan               = var.dx2b-vlan_id
  address_family     = "ipv4"
  bgp_asn            = 65000
  vpn_gateway_id     = aws_vpn_gateway.vgw-b.id
  bgp_auth_key       = var.bgp-auth-key
  customer_address   = "10.0.0.13/30"
  amazon_address     = "10.0.0.14/30"
}

